import React from 'react'

export const ExampleProps = (props) => {
  const {children} = props
  return (
    <p>
      {children}
    </p>
  )
}
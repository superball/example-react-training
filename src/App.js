import React from 'react';
import logo from './logo.svg';
import { ExampleProps } from './exampleProps'
import './App.css';

class App extends React.Component {
  state = {
    test: ''
  }
  onChangeText = (event) => {
    const { value, name } = event.target
    console.log(value, name)
    console.log('state input ===>', this.state.test)
    this.setState({
      [name]: value
    })
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
        </p>
          <p>
            {this.state.test}
          </p>
          <ExampleProps>
            {this.state.test}
          </ExampleProps>
          <input name="test" onChange={this.onChangeText} value={this.state.test} />
          {/* <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React TT
        </a> */}
        </header>

      </div>
    )
  }
}

export default App;

import React from 'react'
import { Button, Input } from '../../components'

class TodoListContainer extends React.Component {
  state = {
    test: '',
    testList: []
  }
  toggleChangeColor = () => {
    this.setState({
      color: 'red'
    })
  }
  onChange = (event) => {
    const { value, name } = event.target
    this.setState({
      [name]: value
    })
  }
  onSubmit = (event) => {
    let updateTestList = this.state.testList
    updateTestList.push(this.state.test)
    this.setState({
      test: '',
      testList: updateTestList
    })
    event.preventDefault()

  }
  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <Input id="test" name="test" value={this.state.test} onChange={this.onChange} />
          {/* <Button color={this.state.color} onClick={this.toggleChangeColor}>Test Click !!</Button>
        <Button>Test Click2 !!</Button> */}
        </form>
        {
          this.state.testList.map((element,index) => {
            return (
              <p key={index}>{element}</p>
            )
          })
        }
      </div>
    )
  }
}

export default TodoListContainer
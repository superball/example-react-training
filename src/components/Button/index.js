import React from 'react'
import { ButtonStyles } from './styles'

export const Button = (props) => {
  const { children, onClick, color } = props
  return (
    <ButtonStyles onClick={onClick} color={color}>
      {children}
    </ButtonStyles>
  )
}

Button.defaultProps = {
  color: 'green'
}
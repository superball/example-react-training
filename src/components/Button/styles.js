import styled from 'styled-components'

export const ButtonStyles = styled.button`
  background-color: ${props => props.color};
  color: #FFF;
  font-size: 24px;
`
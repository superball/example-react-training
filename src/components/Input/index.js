import React from 'react'
import { InputStyle } from './styles'

export const Input = (props) => {
    const { value, name, onChange } = props
    return (
        <InputStyle value={value} name={name} onChange={onChange} />
    )
}
import styled from 'styled-components'

export const InputStyle = styled.input`
    border: 1px solid grey;
    height: 50px;
    width: 50%;
    font-size: 24px;
    border-radius: 5px;
    /* line-height: */
`